import pytest
from midb import PObject, MemoryObject, get_backend, setup_pobject


# MemoryObject.__repr__()
def test_memory_object_repr():
    mo = MemoryObject(a='a', b=2, c=False)
    assert repr(mo) == "MemoryObject(a = 'a', b = 2, c = False)"


# MemoryObject.__eq__()
def test_memory_object_eq_are_equal():
    mo1 = MemoryObject(a=1, b=2, c=3)
    mo2 = MemoryObject(a=1, b=2, c=3)
    assert mo1 == mo2


def test_memory_object_eq_are_not_equal_number_of_attributes():
    mo1 = MemoryObject(a=1, b=2, c=3)
    mo2 = MemoryObject(a=1, b=2)
    assert not (mo1 == mo2)
    assert mo1 != mo2


def test_memory_object_eq_are_not_equal_names_of_attributes():
    mo1 = MemoryObject(a=1, b=2, c=3)
    mo2 = MemoryObject(x=1, y=2, z=3)
    assert not (mo1 == mo2)
    assert mo1 != mo2


def test_memory_object_eq_are_not_equal_value_of_attributes():
    mo1 = MemoryObject(a=1, b=2, c=3)
    mo2 = MemoryObject(a=4, b=5, c=6)
    assert not (mo1 == mo2)
    assert mo1 != mo2


def test_memory_object_eq_are_not_equal_different_types():
    class MyMemoryObject(MemoryObject):
        pass

    mo1 = MemoryObject(a=1, b=2, c=3)
    mo2 = MyMemoryObject(a=1, b=2, c=3)
    assert not (mo1 == mo2)
    assert mo1 != mo2


# PObject.__init__()
def test_init_default():
    o = PObject()
    assert o._backend is None
    assert o._id is None
    assert o._temp is not None
    assert type(o._temp) is PObject()._in_memory_class


def test_init_some_values():
    o = PObject(a=1, b=2)
    assert o.a == 1
    assert o.b == 2
    assert o._backend is None
    assert o._id is None
    assert o._temp is not None
    assert type(o._temp) is PObject()._in_memory_class


def test_init_with_backend(memory_backend):
    o = PObject(a=1, b=2, _backend=memory_backend)
    _id = o._id
    assert o._id == 1
    assert o._backend == memory_backend
    assert o._temp is None

    memory_backend.root['test'] = o

    assert memory_backend.root['test'] == o
    assert memory_backend.root['test']._id == o._id == _id
    assert memory_backend.root['test']._backend == o._backend == memory_backend
    assert memory_backend.root['test']._temp == o._temp
    assert memory_backend.root['test']._temp is None


def test_init_id_only():
    o = PObject(_id=1)
    assert o._id == 1
    assert o._backend is None
    assert o._temp is None


# PObject.__getattr__(); PObject.o
def test_getattr_temp():
    o = PObject(a=1)
    assert o.still_temp()
    assert o.a == 1


def test_getattr_temp_no_attribute():
    o = PObject(a=1)
    assert o.still_temp()
    with pytest.raises(AttributeError):
        o.somethingelse


def test_getattr_persistent(memory_root):
    o = PObject(a=1)
    memory_root['test'] = o
    assert not o.still_temp()
    assert o.a == 1


def test_getattr_persistent_no_attribute(memory_root):
    o = PObject(a=1)
    memory_root['test'] = o
    assert not o.still_temp()
    with pytest.raises(AttributeError):
        o.somethingelse



# PObject._get_temp()
def test_get_temp():
    o = PObject(a=1)
    assert o._get('a') == 1

# PObject.__setattr__(); PObject.o = value
def test_setattr_temp():
    o = PObject()
    o.a = 1
    assert o.still_temp()
    assert o.a == 1


def test_setattr_persistent(memory_root):
    o = PObject()
    memory_root['test'] = o
    o.a = 1
    assert not o.still_temp()
    assert o.a == 1


# PObject._set_temp()
def test_set_temp():
    o = PObject()
    o._set('a', 1)
    assert o.a == 1


# PObject.__delttr__(); del PObject.o
def test_delattr_temp():
    o = PObject(a=1)
    assert o.still_temp()
    assert o.a == 1

    del o.a

    assert o.still_temp()
    with pytest.raises(AttributeError):
        o.a


def test_delattr_persistent(memory_root):
    o = PObject(a=1)
    memory_root['test'] = o
    assert not o.still_temp()
    assert o.a == 1

    del o.a

    assert not o.still_temp()
    with pytest.raises(AttributeError):
        o.a


def test_delattr_reserved(memory_root):
    o = PObject(a=1)
    memory_root['test'] = o
    assert o._backend is not None
    del o._backend
    assert not hasattr(o, "_backend")


# custom object
def test_simple_custom_pobject():
    class MyObj(MemoryObject):
        def __init__(self, a=None, b=None):
            self.a = a
            self.b = b

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj(a=1, b=2)
    assert type(backend.root['test']) == PMyObj
    assert backend.root['test'].a == 1
    assert backend.root['test'].b == 2
    assert type(backend.root['test'].in_memory()) == MyObj
    assert backend.root['test'].in_memory() == MyObj(a=1, b=2)


def test_custom_pobject_without_default_values():
    class MyObj(MemoryObject):
        def __init__(self, a, b):
            self.a = a
            self.b = b

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj(a=1, b=2)
    assert type(backend.root['test']) == PMyObj
    assert backend.root['test'].a == 1
    assert backend.root['test'].b == 2
    assert type(backend.root['test'].in_memory()) == MyObj
    assert backend.root['test'].in_memory() == MyObj(a=1, b=2)


def test_custom_pobject_whos_inmemory_object_constructor_have_arguments_that_match_attributes():
    class MyObj(MemoryObject):
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj(a_value=1, b_value=2)
    assert type(backend.root['test']) == PMyObj
    assert backend.root['test'].a == 1
    assert backend.root['test'].b == 2
    assert type(backend.root['test'].in_memory()) == MyObj
    assert backend.root['test'].in_memory() == MyObj(1, 2)

def test_custom_pobject_with_positional_arguments():
    class MyObj(MemoryObject):
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj(1, 2)
    assert type(backend.root['test']) == PMyObj
    assert backend.root['test'].a == 1
    assert backend.root['test'].b == 2
    assert type(backend.root['test'].in_memory()) == MyObj
    assert backend.root['test'].in_memory() == MyObj(1, 2)


def test_custom_pobject_inmemory_function():
    class MyObj(MemoryObject):
        def test_func(self):
            return "test_func"

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj()
    assert type(backend.root['test']) == PMyObj
    assert backend.root['test'].test_func() == 'test_func'

def test_custom_pobject_inmemory_property_function():
    class MyObj(MemoryObject):
        @property
        def test_prop(self):
            return "test_prop"

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj()
    assert type(backend.root['test']) == PMyObj
    assert backend.root['test'].test_prop == 'test_prop'


def test_custom_pobject_in_memory_object_has_property_function_and_setter():
    class MyObj(MemoryObject):
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

        @property
        def test_prop(self):
            return self.value

        @test_prop.setter
        def test_prop(self, value):
            self.value = value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj('A', "B")
    with pytest.raises(AttributeError):
        backend.root['test'].test_prop
    backend.root['test'].test_prop = 'test_prop value'
    assert backend.root['test'].value == 'test_prop value'
    assert backend.root['test'].test_prop == 'test_prop value'


def test_custom_pobject_in_memory_property_function_and_setter_temp():
    class MyObj(MemoryObject):
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

        @property
        def test_prop(self):
            return self.value

        @test_prop.setter
        def test_prop(self, value):
            self.value = value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    o = PMyObj('A', "B")
    assert o._id is None
    assert o._backend is None
    assert o._temp is not None
    assert type(o._temp) is MyObj
    o.test_prop = 1
    assert o.test_prop == 1
    assert o._temp.test_prop == 1
    assert o.value == 1
    assert o._temp.value == 1


def test_custom_without_subclassing_memory_object(memory_root):
    class MyObj:
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    memory_root['test'] = MyObj('a', 'b')
    assert memory_root['test'].a == 'a'
    assert memory_root['test'].b == 'b'


def test_custom_direct_set(memory_root):
    class MyObj(MemoryObject):
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    memory_root['test'] = MyObj('a', 'b')
    assert memory_root['test'].a == 'a'
    assert memory_root['test'].b == 'b'
    assert memory_root['test'] == MyObj('a', 'b')
    assert MyObj('a', 'b') == memory_root['test']