# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import random
import pytest
from midb import get_root
from midb.persistent_objects import PList

initial_values = [1, 2.2, 'test', True]


class TempPList(PList):
    pass


list_types = [list, TempPList, PList]


def initialize_list(list_type, contents, root=None, root_key='test'):
    if list_type == PList:
        root[root_key] = list_type(tuple(contents))
        return root[root_key]
    else:
        return list_type(contents)



@pytest.fixture(params=list_types)
def list_type(request):
    yield request.param


@pytest.fixture(scope='function')
def initialized_list(list_type, memory_root):
    yield initialize_list(list_type, initial_values, memory_root)


# list(iterable=())
def test_default_constructor(list_type):
    l = list_type()
    assert l == []


def test_constructor_with_iterator(list_type):
    l = list_type(initial_values)
    assert l == initial_values


#__add__(value)
def test_add(initialized_list):
    l = initialized_list + initialized_list
    assert l == initial_values + initial_values
    assert len(l) == 2*len(initial_values)


# __contains__(key)
def test_contains(initialized_list):
    assert initial_values[0] in initialized_list


# __delitem__(key)
def test_delitem(memory_root, list_type):
    initialized_list = initialize_list(list_type, list(initial_values), memory_root)
    values = list(initial_values)
    del initialized_list[2]
    del values[2]
    assert initialized_list == values


def test_delitem_out_of_range(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    with pytest.raises(IndexError):
        del test_list[len(reference)]
    with pytest.raises(IndexError):
        del test_list[-(len(reference) + 1)]



# __eq__(value)
def test_eq(initialized_list):
    assert initialized_list == initial_values


# __ge__(value)
def test_ge(memory_root, list_type):
    initial_values_a = list([random.choice('ab') for _ in range(2)])
    initial_values_b = list([random.choice('ab') for _ in range(2)])
    test_list_a = initialize_list(list_type, initial_values_a, memory_root, "test1")
    test_list_b = initialize_list(list_type, initial_values_b, memory_root, "test2")
    assert (initial_values_a >= initial_values_b) == (test_list_a >= test_list_b)


# __getattribute__(name)
def test_getattr(initialized_list):
    assert getattr(initialized_list, '__len__')() == len(initial_values)


# __getitem__(...)
def test_getitem(initialized_list):
    for i in range(len(initialized_list)):
        assert initialized_list[i] == initial_values[i]


# __gt__(value)
def test_gt(memory_root, list_type):
    initial_values_a = list([random.choice('ab') for _ in range(2)])
    initial_values_b = list([random.choice('ab') for _ in range(2)])
    test_list_a = initialize_list(list_type, initial_values_a, memory_root, 'test1')
    test_list_b = initialize_list(list_type, initial_values_b, memory_root, 'test2')
    assert (initial_values_a > initial_values_b) == (test_list_a > test_list_b)


# __add__(value)
# __radd__(value)
def test_add_radd(memory_root, list_type):
    initial_values_a = list([random.choice('abc') for _ in range(3)])
    initial_values_b = list([random.choice('abc') for _ in range(3)])
    test_list_a = initialize_list(list_type, initial_values_a, memory_root, 'test1')
    test_list_b = initialize_list(list_type, initial_values_b, memory_root, 'test2')
    assert (initial_values_a + initial_values_b) == (test_list_a + test_list_b)
    assert (initial_values_a + initial_values_b) == (test_list_a + initial_values_b)
    assert (initial_values_a + initial_values_b) == (initial_values_a + test_list_b)


def test_iadd(memory_root, list_type):
    initial_values_copy = tuple(initial_values)
    initialized_list = initialize_list(list_type, initial_values_copy, memory_root)
    additional_values = list([random.choice('abc') for _ in range(3)])
    initialized_list += additional_values
    assert initialized_list == list(initial_values_copy) + additional_values


# __imul__(value)

# __init__()
# __iter__()
# __le__(value)
def test_le(memory_root, list_type):
    initial_values_a = list([random.choice('ab') for _ in range(2)])
    initial_values_b = list([random.choice('ab') for _ in range(2)])
    test_list_a = initialize_list(list_type, initial_values_a, memory_root, 'test1')
    test_list_b = initialize_list(list_type, initial_values_b, memory_root, 'test2')
    assert (initial_values_a <= initial_values_b) == (test_list_a <= test_list_b)
# __len__()


# __lt__(value)
def test_lt(memory_root, list_type):
    initial_values_a = list([random.choice('ab') for _ in range(2)])
    initial_values_b = list([random.choice('ab') for _ in range(2)])
    test_list_a = initialize_list(list_type, initial_values_a, memory_root, 'test1')
    test_list_b = initialize_list(list_type, initial_values_b, memory_root, 'test2')
    assert (initial_values_a < initial_values_b) == (test_list_a < test_list_b)

# __mul__(value)
def test_mul(memory_root, list_type):
    initial_values= list([random.choice('abcd') for _ in range(2)])
    test_list = initialize_list(list_type, initial_values, memory_root)
    multiplyer = random.randint(1, 10)
    assert test_list * multiplyer == initial_values * multiplyer


# __ne__(value)
def test_ne(initialized_list):
    assert initialized_list != initial_values[0:-1]

# __repr__()
def test_repr(initialized_list):
    assert repr(initial_values) in repr(initialized_list)

# __reversed__()
def test_reversed(memory_root, list_type):
    values = [random.choice('abcdefg') for _ in range(10)]
    test_list = initialize_list(list_type, values, memory_root)
    reversed_values = reversed(values)
    reversed_list = reversed(test_list)
    for v1, v2 in zip(reversed_values, reversed_list):
        assert v1 == v2

# __rmul(value)
def test_rmul(memory_root, list_type):
    initial_values = list([random.choice('abcd') for _ in range(2)])
    test_list = initialize_list(list_type, initial_values, memory_root)
    multiplyer = random.randint(1, 10)
    assert multiplyer * test_list == multiplyer * initial_values


# __setitem__(key, value), l[key] = value
def test_setitem(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    key = random.randint(0,len(reference)-1)
    value = 'a'
    reference[key] = value
    test_list[key] = value
    assert test_list == reference
    assert test_list[key] == value


@pytest.mark.parametrize("key", [-1, 0, 1])
def test_setitem_special_values(memory_root, list_type, key):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    value = 'a'
    reference[key] = value
    test_list[key] = value
    assert test_list == reference
    assert test_list[key] == value

def test_setitem_out_of_range(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    with pytest.raises(IndexError) as exception_info:
        # 1 too big (positive)
        test_list[len(reference)] = 'a'
    error_message = f'{test_list.__class__.__name__} assignment index out of range'
    assert exception_info.value.args[0] == error_message

    with pytest.raises(IndexError) as exception_info:
        # 1 too negative
        test_list[-(len(reference) + 1)] = 'a'
    assert exception_info.value.args[0] == error_message

def test_setitem_out_of_range_empty(memory_root, list_type):
    reference = []
    test_list = initialize_list(list_type, reference, memory_root)
    with pytest.raises(IndexError) as exception_info:
        test_list[0] = 'a'

def test_setitem_slice(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    reference[2:5] = ('a', 'b', 'c')
    test_list[2:5] = ('a', 'b', 'c')
    assert test_list == reference == ['0', '1', 'a', 'b', 'c', '5', '6', '7', '8', '9']


def test_setitem_slice_assign_non_iterator(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    with pytest.raises(TypeError) as exception_info:
        test_list[2:5] = 1
    assert exception_info.value.args[0] == 'can only assign an iterable'


def test_setitem_extended_slice(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    reference[2:7:2] = ('a', 'b', 'c')
    test_list[2:7:2] = ('a', 'b', 'c')
    assert test_list == reference == ['0', '1', 'a', '3', 'b', '5', 'c', '7', '8', '9']


def test_setitem_extended_slice_assignment_wrong_len(memory_root, list_type):
    reference = list('0123456789')
    test_list = initialize_list(list_type, reference, memory_root)
    with pytest.raises(ValueError):
        test_list[2:7:2] = ('a', 'b')


# append(object)
def test_append(initialized_list):
    values = list(initial_values)
    append_value = 'test_append'
    len_list = len(values)
    values.append(append_value)
    initialized_list.append(append_value)
    assert initialized_list == values
    assert len(initialized_list) == len_list + 1
    assert initialized_list[-1] == append_value


# clear()
def test_clear(initialized_list):
    initialized_list.clear()
    assert initialized_list == []

# copy()
def test_copy(initialized_list):
    copy_list = initialized_list.copy()
    assert copy_list == initialized_list
    assert copy_list is not initialized_list

# count(value)
def test_count(memory_root, list_type):
    a_count = random.randint(0,10)
    b_count = random.randint(0,10)
    initial_contents = (('a',) * a_count) + (('b',) * b_count)
    l = initialize_list(list_type, initial_contents, memory_root)
    assert l.count('a') == a_count
    assert l.count('b') == b_count

# extend(iterable)
def test_extend(initialized_list):
    list_original_len = len(initialized_list)
    extend_values = [random.choice('abc') for _ in range(random.randint(3,10))]
    extend_len = len(extend_values)
    initialized_list.extend(extend_values)
    assert len(initialized_list) == list_original_len + extend_len

# index(value, start=0, stop=...)
def test_index(memory_root, list_type):
    contents = list('0123456789')
    test_index = random.randint(0,10)
    test_value = 'a'
    contents.insert(test_index, test_value)
    l = initialize_list(list_type, contents, memory_root)
    assert l.index(test_value) == test_index

# insert(index, object)
def test_insert(memory_root, list_type):
    contents = list('0123456789')
    test_index = random.randint(0, 10)
    test_value = 'a'
    l = initialize_list(list_type, contents, memory_root)
    contents.insert(test_index, test_value)
    l.insert(test_index, test_value)
    assert l == contents
    assert l.index(test_value) == test_index


# pop(index=-1)
def test_pop_default(memory_root, list_type):
    contents = list('0123456789')
    l = initialize_list(list_type, contents, memory_root)
    contents_pop_value = contents.pop()
    l_pop_value = l.pop()
    assert l_pop_value == contents_pop_value
    assert l == contents


def test_pop_random(memory_root, list_type):
    contents = list('0123456789')
    l = initialize_list(list_type, contents, memory_root)
    test_index = random.randint(-len(contents), len(contents)-1)
    contents_pop_value = contents.pop(test_index)
    l_pop_value = l.pop(test_index)
    assert l_pop_value == contents_pop_value
    assert l == contents


@pytest.mark.parametrize("test_index", [-1, 0, 1])
def test_pop_special_values(memory_root, list_type, test_index):
    contents = list('0123456789')
    l = initialize_list(list_type, contents, memory_root)
    contents_pop_value = contents.pop(test_index)
    l_pop_value = l.pop(test_index)
    assert l_pop_value == contents_pop_value
    assert l == contents

def test_pop_error_empty(memory_root, list_type):
    contents = []
    l = initialize_list(list_type, contents, memory_root)
    with pytest.raises(IndexError) as exception_info:
        v = l.pop()
    assert exception_info.value.args[0] == f"pop from empty {l.__class__.__name__}"


def test_pop_error_out_of_range(memory_root, list_type):
    contents = [1, 2, 3]
    l = initialize_list(list_type, contents, memory_root)
    with pytest.raises(IndexError) as exception_info:
        v = l.pop(3)
    assert exception_info.value.args[0] == "pop index out of range"



# remove(value)
def test_remove(memory_root, list_type):
    contents = list('0123456789')
    l = initialize_list(list_type, contents, memory_root)
    value = random.choice(contents)
    contents.remove(value)
    l.remove(value)
    assert l == contents


# reverse()
def test_reversed(memory_root, list_type):
    contents = list('0123456789')
    l = initialize_list(list_type, contents, memory_root)
    assert list(reversed(l)) == list(reversed(contents))
    assert list(reversed(l)) == list('9876543210')

# sort(key=None, reversed=False)
def test_sort(memory_root, list_type):
    contents = list('gfedcba')
    l = initialize_list(list_type, contents, memory_root)
    contents.sort()
    l.sort()
    assert l == contents
    assert l == list('abcdefg')