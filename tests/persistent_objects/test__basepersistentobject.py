# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import pytest
from midb import get_backend
from midb.backend import SQLiteBackend
from midb.persistent_objects import PDict, BasePersistentObject, PTuple, PList
from midb.persistent_objects import MemoryObject, PObject, setup_pobject


# __init__
def test_init_from_dict_only():
    init_value = {'test1': 'A'}
    obj = PDict(init_value)
    assert obj['test1'] == 'A'
    assert obj._backend is None
    assert obj._id is None
    assert obj._temp == init_value


def test_init_from_dict_with_backend():
    backend = SQLiteBackend()
    init_value = {'test1': 'A'}
    obj = PDict(init_value, _backend=backend)
    assert obj._backend == backend
    assert obj._id is not None
    assert obj._temp is None
    assert obj == init_value


# __getattribute__
def test_getattribute_proxied_attribute():
    obj = PDict({'test1': 'A'})
    with pytest.raises(AttributeError):
        key_attribute = super(BasePersistentObject, obj).__getattribute__('keys')
    assert list(obj.keys()) == ['test1']


def test_non_existent_attribute():
    obj = PDict({'test1': 'A'})
    with pytest.raises(AttributeError):
        obj.yatta_yatta

# _string_serializer
def test_string_serializer_wrong_class():
    obj = PDict()
    with pytest.raises(ValueError):
        obj._string_serializer(1)


# still_temp
def test_still_temp():
    root = SQLiteBackend().root
    obj = PDict({'test': 1})
    assert obj.still_temp() is True
    assert obj._temp is not None
    root['test'] = obj
    assert obj.still_temp() is False
    assert obj._temp is None


# __setattr__
def test_setattr_not_reserved():
    obj = PDict({'test1': 'A'})
    with pytest.raises(AttributeError):
        obj.yatta_yatta = 'test'


def test_setattr_set_temp_but_already_fully_initialized(memory_root):
    memory_root['test'] = {'test': 'A'}
    obj = memory_root['test']
    assert obj._temp is None
    assert obj._id == 1
    assert obj._backend == memory_root._backend
    obj._temp = {'test2': 'B'}  # this should do nothing
    assert obj._temp == None
    assert obj._id == 1
    assert obj._backend == memory_root._backend


def test_setattr_set_backend_to_illegal_value(memory_root):
    _backend = memory_root._backend
    memory_root._backend = 1  # should do nothing
    assert memory_root._backend == _backend


# _get
def test_get_int(memory_root):
    memory_root['test'] = 1
    assert memory_root._get('test') == 1


def test_get_dict(memory_root):
    init = {'a': 1, 2: True, False: None}
    memory_root['test'] = init
    result = memory_root._get('test')
    assert type(result) == PDict
    assert result == init
    assert result._backend == memory_root._backend
    assert result._id == 1
    assert result._temp == None


def test_get_tuple(memory_root):
    init = (1, 'a', 3.2, True, None)
    memory_root['test'] = init
    result = memory_root._get('test')
    assert result == init
    assert result._backend == memory_root._backend
    assert result._id == 1
    assert result._temp == None

# _set
def test_set_dictionary_value():
    obj = PDict()
    obj['test1'] = {'test1': 1}
    assert  type(obj['test1']) == PDict
    assert obj['test1']['test1'] == 1


def test_set_tuple_value():
    obj = PDict()
    obj['test1'] = ('test2',)
    assert type(obj['test1']) == PTuple
    assert obj['test1'][0] == 'test2'


def test_set_list_value():
    obj = PDict()
    obj['test1'] = ['test2']
    assert type(obj['test1']) == PList
    assert obj['test1'][0] == 'test2'


def test_set_existing_object(memory_root):
    contents = {'a': 1}
    memory_root['test1'] = contents
    _id = memory_root['test1']._id
    memory_root['test2'] = memory_root['test1']
    assert memory_root['test2']._id == memory_root['test2']._id == _id


def test_set_persistent_object_from_another_db(memory_root, memory_root2):
    memory_root["a persistent object to make sure that the 2 files don't aline"] = {'nothing really': None}
    contents = {'a': 1}
    memory_root['test1'] = contents

    memory_root2['test2'] = memory_root['test1']

    assert memory_root2['test2'] == memory_root['test1'] == contents
    assert memory_root2['test2']._id != memory_root['test1']._id


def test_reset_persistent_object():
    backend = SQLiteBackend()
    root = backend.root

    root['test'] = {'test_a': 'a'}
    first_id = root['test']._id

    root['test'] = {'test_b': 'b'}
    second_id = root['test']._id

    assert root['test'] == {'test_b': 'b'}

    assert first_id != second_id
    with pytest.raises(KeyError):
        backend.get(first_id, 'test_a')

def test_reset_persistent_object_where_key_is_first_value():
    backend = SQLiteBackend()
    root = backend.root
    key = PTuple(('a', 'b', 'c'))
    root[key] = key
    first_id = key._id

    root[key] = {'test_b': 'b'}
    second_id = root[key]._id

    assert root[key] == {'test_b': 'b'}
    assert first_id != second_id
    assert key._id == first_id
    assert key._backend == root._backend
    assert key._temp == None
    assert key == ('a', 'b', 'c')
    assert backend.get(first_id, 0) == 'a'
    assert backend.get(first_id, 1) == 'b'
    assert backend.get(first_id, 2) == 'c'

def test_set_reset_persistent_object():
    backend = SQLiteBackend()
    root = backend.root
    root['test1'] = {'testA': 1}
    obj1 = root['test1']
    root['test1'] = {'testB': 2}
    obj2 = root['test1']
    assert root['test1']['testB'] == 2


def test_set_persistent_object_with_different_backend():
    backend1 = SQLiteBackend()
    backend2 = SQLiteBackend()
    initial_data = {'testA': 'a'}
    backend1.root['test1'] = initial_data
    backend2.root['test2'] = backend1.root['test1']
    assert backend1.root['test1'] == backend2.root['test2']
    assert backend1.root['test1'] is not backend2.root['test2']


# _del
def test_del_of_temp():
    temp_obj = PDict({'a': 1, 'b': 2})
    del temp_obj['a']
    assert temp_obj == {'b': 2}


def test_del_argument_default_true(backend1, backend2):
    backend1.root['test'] = PDict({'test': 1})
    assert backend2.root['test']['test'] == 1
    del backend1.root['test']['test']
    with pytest.raises(KeyError):
        backend2.root['test']['test']


# _set, _set, _del
def test_get_set_del_tuple_key(memory_root):
    hashable_tuple = (1, 2)
    with pytest.raises(KeyError):
        memory_root[hashable_tuple]

    memory_root[hashable_tuple] = 1

    assert memory_root[hashable_tuple] == 1

    del memory_root[hashable_tuple]

    with pytest.raises(KeyError):
        memory_root[hashable_tuple]


def test_get_set_del_hashable_custom_object_key(memory_root):
    class A(MemoryObject):
        def __init__(self, a):
            self.a = a

        def __hash__(self):
            return hash(self.a)

    @setup_pobject
    class PA(PObject):
        _in_memory_class = A

    hashable_object = A(1)
    with pytest.raises(KeyError):
        memory_root[hashable_object]

    memory_root[hashable_object] = 1

    assert memory_root[hashable_object] == 1

    del memory_root[hashable_object]

    with pytest.raises(KeyError):
        memory_root[hashable_object]


# o.__contains__(other); other in o
def test_contains(file_backend):
    backend = file_backend
    backend.root['test'] = PDict({'test_sub': 1})
    assert 'test_sub' in backend.root['test']
    assert 'test_sub_other' not in backend.root['test']


# o.__ge__(other); o >= other
def test_ge(memory_backend):
    r = memory_backend.root
    first = (1,)
    r['first'] = first
    p_first = r['first']
    second_eq = (1,)
    r['second_eq'] = second_eq
    p_second_eq = r['second_eq']
    second_gt = (2,)
    r['second_gt'] = second_gt
    p_second_gt = r['second_gt']

    assert second_eq >= first
    assert second_gt >= first

    assert p_second_eq >= p_first
    assert p_second_gt >= p_first

    assert p_second_eq >= first
    assert p_second_gt >= first

    assert second_eq >= p_first
    assert second_gt >= p_first


def test_ge_error(memory_backend):
    backend = memory_backend
    initial_values = {'test_sub': 1}
    backend.root['test'] = PDict(initial_values)
    backend.root['test2'] = PDict(initial_values)
    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] >= initial_values
    assert str(exception_info.value) == "'>=' not supported between instances of 'PDict' and 'dict'"

    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] >= backend.root['test2']
    assert str(exception_info.value) == "'>=' not supported between instances of 'PDict' and 'PDict'"


# o.__gt__(other); o > other
def test_gt(memory_backend):
    r = memory_backend.root
    first = (1,)
    r['first'] = first
    p_first = r['first']
    second_eq = (1,)
    r['second_eq'] = second_eq
    p_second_eq = r['second_eq']
    second_gt = (2,)
    r['second_gt'] = second_gt
    p_second_gt = r['second_gt']

    assert not(second_eq > first)
    assert second_gt > first

    assert not(p_second_eq > p_first)
    assert p_second_gt > p_first

    assert not(p_second_eq > first)
    assert p_second_gt > first

    assert not(second_eq > p_first)
    assert second_gt > p_first

def test_gt_error():
    backend = SQLiteBackend()
    initial_values = {'test_sub': 1}
    backend.root['test'] = PDict(initial_values)
    backend.root['test2'] = PDict(initial_values)
    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] > initial_values
    assert str(exception_info.value) == "'>' not supported between instances of 'PDict' and 'dict'"

    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] > backend.root['test2']
    assert str(exception_info.value) == "'>' not supported between instances of 'PDict' and 'PDict'"

# o.__iter__(); iter(o)
def test_iter():
    backend = SQLiteBackend()
    initial_values = {'test_sub1': 1, 'test_sub2': 2}
    keys = tuple(initial_values.keys())
    backend.root['test'] = PDict(initial_values)
    for v in backend.root['test']:
        assert v in keys

# o.__le__(other); o <= other
def test_le(memory_backend):
    r = memory_backend.root
    first = (2,)
    r['first'] = first
    p_first = r['first']
    second_eq = (2,)
    r['second_eq'] = second_eq
    p_second_eq = r['second_eq']
    second_gt = (1,)
    r['second_gt'] = second_gt
    p_second_gt = r['second_gt']

    assert second_eq <= first
    assert second_gt <= first

    assert p_second_eq <= p_first
    assert p_second_gt <= p_first

    assert p_second_eq <= first
    assert p_second_gt <= first

    assert second_eq <= p_first
    assert second_gt <= p_first


def test_le_error():
    backend = SQLiteBackend()
    initial_values = {'test_sub': 1}
    backend.root['test'] = PDict(initial_values)
    backend.root['test2'] = PDict(initial_values)
    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] <= initial_values
    assert str(exception_info.value) == "'<=' not supported between instances of 'PDict' and 'dict'"

    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] <= backend.root['test2']
    assert str(exception_info.value) == "'<=' not supported between instances of 'PDict' and 'PDict'"


# o.__len__(); len(o)
def test_len():
    backend = SQLiteBackend()
    initial_values = {'test_sub1': 1, 'test_sub2': 2}
    backend.root['test'] = PDict(initial_values)
    assert len(backend.root['test']) == len(initial_values)


# o.__lt__(other); o < other
def test_lt(memory_backend):
    r = memory_backend.root
    first = (2,)
    r['first'] = first
    p_first = r['first']
    second_eq = (2,)
    r['second_eq'] = second_eq
    p_second_eq = r['second_eq']
    second_gt = (1,)
    r['second_gt'] = second_gt
    p_second_gt = r['second_gt']

    assert not(second_eq < first)
    assert second_gt < first

    assert not(p_second_eq < p_first)
    assert p_second_gt < p_first

    assert not(p_second_eq < first)
    assert p_second_gt < first

    assert not(second_eq < p_first)
    assert second_gt < p_first


def test_lt_error():
    backend = SQLiteBackend()
    initial_values = {'test_sub': 1}
    backend.root['test'] = PDict(initial_values)
    backend.root['test2'] = PDict(initial_values)
    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] < initial_values
    assert str(exception_info.value) == "'<' not supported between instances of 'PDict' and 'dict'"

    with pytest.raises(TypeError) as exception_info:
        backend.root['test'] < backend.root['test2']
    assert str(exception_info.value) == "'<' not supported between instances of 'PDict' and 'PDict'"


# o.__ne__(other); o != other
def test_ne():
    backend = SQLiteBackend()
    backend.root['test'] = PDict({'test_sub': 1})
    backend.root['test2'] = PDict({'test_sub2': 2})
    assert backend.root['test'] != backend.root['test2']
    assert not(backend.root['test'] != backend.root['test'])


# d.__repr__; repr(d)
def test_repr():
    backend = SQLiteBackend()
    initial_values = {'test_sub1': 1, 'test_sub2': 2}
    backend.root['test'] = PDict(initial_values)
    assert repr(initial_values) in repr(backend.root['test'])


def test_repr_temp():
    d = PDict()
    assert repr(d) == "PDict({}, _backend=None, _id=None, _temp={})"


def test_repr_persistent(memory_root):
    assert repr(memory_root) == 'PDict({}, _backend=SQLiteBackend(filename=":memory:"), _id=0, _temp=None)'



def test_repr_attributes_not_set():
    d = PDict()
    del d._backend
    del d._id
    del d._temp
    assert repr(d) == "PDict((no contents), (_backend not set), (_id not set), (_temp not set))"


# d.__reversed__();  reversed(d)
def test_reversed():
    backend = SQLiteBackend()
    initial_values = {'test_sub1': 1, 'test_sub2': 2}
    backend.root['test'] = PDict(initial_values)
    for a, b in zip(reversed(initial_values), reversed(backend.root['test'])):
        assert a == b


# General tests
def test_nested_persistent_objects():
    backend = SQLiteBackend()
    obj = PDict(_backend=backend, _id=0)
    obj['test3'] = PDict({'test1': 1, 'test2': 2})
    assert isinstance(obj['test3'], PDict)
    assert obj['test3']['test1'] == 1
    assert obj['test3']['test2'] == 2


# test custom object with properties
def test_custom_pobject_in_memory_object_has_property_function_and_setter():
    class MyObj(MemoryObject):
        def __init__(self, a_value, b_value):
            self.a = a_value
            self.b = b_value

        @property
        def test_prop(self):
            return self.value

        @test_prop.setter
        def test_prop(self, value):
            self.value = value

    @setup_pobject
    class PMyObj(PObject):
        _in_memory_class = MyObj

    backend = get_backend()
    backend.root['test'] = PMyObj('A', "B")
    with pytest.raises(AttributeError):
        backend.root['test'].test_prop
    backend.root['test'].test_prop = 'test_prop value'
    assert backend.root['test'].value == 'test_prop value'
    assert backend.root['test'].test_prop == 'test_prop value'