# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from midb import PObject


class SampleObject:
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class TempPObject(PObject):
    ...


initial_values = {'a': 1,
                  'b': 2.2,
                  'c': 'test',
                  'd': True}

object_types = [SampleObject, TempPObject, PObject]


@pytest.fixture(params=object_types)
def object_type(request):
    yield request.param


def initialize_object(object_type, contents, root, root_key='test'):
    if object_type == PObject:
        root[root_key] = object_type(**contents)
        return root[root_key]
    else:
        return object_type(**contents)


@pytest.fixture(scope='function')
def initialized_object(object_type, memory_root):
    yield initialize_object(object_type, initial_values, memory_root)


# o.__getattr__(name), o.name
def test_getattr(initialized_object):
    for key in initial_values:
        assert getattr(initialized_object, key) == initial_values[key]


def test_getattr_simple(initialized_object):
    assert initialized_object.a == 1


# o.__setattr__(name, value), o.name = value
def test_setattr(object_type, memory_root):
    test_object = initialize_object(object_type, {}, memory_root)
    test_object.a = 1
    assert test_object.a == 1

# o.__delattr__(name), del o.name
def test_delattr(initialized_object):
    key_to_del = list(initial_values.keys())[0]
    assert getattr(initialized_object, key_to_del) == initial_values[key_to_del]
    delattr(initialized_object, key_to_del)
    with pytest.raises(AttributeError):
        getattr(initialized_object, key_to_del)


def test_delattr_simple(initialized_object):
    assert initialized_object.a == initial_values['a']
    del initialized_object.a
    with pytest.raises(AttributeError):
        # noinspection PyStatementEffect
        initialized_object.a



