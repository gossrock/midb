# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest

from midb import get_root
from midb import PDict


class TempPDict(PDict):
    ...


initial_values = {'a': 1,
                  'b': 2.2,
                  'c': 'test',
                  'd': True}


dictionary_types = [dict, TempPDict, PDict]


@pytest.fixture(params=dictionary_types)
def dict_type(request):
    yield request.param


def initialize_dict(dict_type, contents, root=None, root_key='test'):
    if dict_type == PDict:
        root[root_key] = dict_type(contents)
        return root[root_key]
    else:
        return dict_type(contents)


@pytest.fixture(scope='function')
def initialized_dict(dict_type, memory_root):
    yield initialize_dict(dict_type, initial_values, memory_root)

# __init__
def test_default_constructor(dict_type):
    d = dict_type()
    assert type(d) is dict_type
    assert len(d.keys()) == 0


def test_construction_from_mapping(dict_type):
    d = dict_type(initial_values)
    assert d == initial_values
    assert d is not initial_values


def test_construction_from_iterable(dict_type):
    d = dict_type(tuple(initial_values.items()))
    assert d == initial_values
    assert d is not initial_values


def test_construction_from_kwargs(dict_type):
    d = dict_type(**initial_values)
    assert d == initial_values
    assert d is not initial_values


def test_construction_positional_only_argument(dict_type):
    d = dict_type(initial_values=5)
    assert d == {'initial_values': 5}


# __contains__(key); 'key in d'
def test_contains(initialized_dict):
    for key in initial_values.keys():
        assert key in initialized_dict


# __delitem__(key); del d[key]
def test_delitem(initialized_dict):
    key = 'a'
    assert key in initialized_dict.keys()
    del initialized_dict[key]
    assert key not in initialized_dict.keys()


def test_delitem_equivalent_values(dict_type, memory_root):
    d = initialize_dict(dict_type, {0: 0, 1: 1, 2: 2}, memory_root)
    del d[False]
    del d[1.0]
    assert d == {2: 2}


# __eq__(value); ' d == value'
def test_eq(initialized_dict):
    assert initial_values is not initialized_dict
    assert initial_values == initialized_dict


# __ge__(value); d >= value
def test_ge(initialized_dict):
    with pytest.raises(TypeError):
        initialized_dict >= initialized_dict


# __getattribute__(name)

# __getitem__(key); d[key]
def test_getitem(initialized_dict):
    for key, value in initial_values.items():
        assert initialized_dict[key] == value


def test_getitem_equivalent_values(dict_type, memory_root):
    d = initialize_dict(dict_type, {1: 1}, memory_root)
    assert d[1] == 1
    assert d[1.0] == 1
    assert d[True] == 1


# __gt__(value) 'd > value'
def test_gt(initialized_dict):
    with pytest.raises(TypeError):
        initialized_dict > initialized_dict


# __iter__()
def test_iter(initialized_dict):
    for key in initialized_dict:
        assert key in initial_values.keys()


# __le__(value) 'd <= value'
def test_le(initialized_dict):
    with pytest.raises(TypeError):
        initialized_dict <= initialized_dict


# __len__() 'len()'
def test_len(initialized_dict):
    assert len(initialized_dict) == len(initial_values)


# __lt__(value) '<'
def test_lt(initialized_dict):
    with pytest.raises(TypeError):
        initialized_dict < initialized_dict


# __ne__(value) '!='
def test_ne(initialized_dict):
    assert initialized_dict == initial_values
    initialized_dict.popitem()
    assert initialized_dict != initial_values


# __repr__()
def test_repr(initialized_dict):
    assert repr(initial_values) in repr(initialized_dict)


# __reversed__()
def test_reversed(initialized_dict):
    forward = list(iter(initialized_dict))
    backwards = list(reversed(initialized_dict))
    assert forward == list(iter(initial_values))
    assert backwards == list(reversed(initial_values))
    assert backwards == forward[::-1]


# __setattr__(key, value); d.key = value
def test_setattr(initialized_dict):
    with pytest.raises(AttributeError):
        initialized_dict.yada_yada = 1


def test_setitem_equivalent_values(dict_type, memory_root):
    d = initialize_dict(dict_type, {}, memory_root)
    d[1] = 1
    d[1.0] += 1
    d[True] += 1
    assert d[1] == 3


# __setitem__(key, value); d[key] = value
def test_setitem(initialized_dict):
    key, value = 'e', 'f'
    initialized_dict[key] = value
    assert initialized_dict[key] == value


# __sizeof__

# clear()
def test_clear(initialized_dict):
    assert initialized_dict == initial_values
    initialized_dict.clear()
    assert initialized_dict == {}


# copy()
def test_copy(initialized_dict):
    dict_copy = initialized_dict.copy()
    assert dict_copy == initialized_dict
    assert dict_copy is not initialized_dict


# get()
def test_get(initialized_dict):
    assert initialized_dict.get('a', 'z') == initialized_dict['a']
    assert initialized_dict.get('z') is None
    assert initialized_dict.get('z', 'z') == 'z'


def test_get_error_typeerror_no_keyword_arguments(initialized_dict):
    with pytest.raises(TypeError):
        initialized_dict.get('a', default='z')


# items()
def test_items(initialized_dict):
    source_items = list(initial_values.items())
    test_items_list = list(initialized_dict.items())
    assert len(test_items_list) == len(source_items)
    for test_item in test_items_list:
        assert test_item in source_items


# keys()
def test_keys(initialized_dict):
    source_keys = list(initial_values.keys())
    test_keys_list = list(initialized_dict.keys())
    assert len(test_keys_list) == len(source_keys)
    for source_key in source_keys:
        assert source_key in test_keys_list


# pop()
def test_pop(initialized_dict):
    first_key = list(initialized_dict.keys())[0]
    a_value = initialized_dict[first_key]
    popped = initialized_dict.pop(first_key)
    assert popped == a_value
    with pytest.raises(KeyError):
        # noinspection PyStatementEffect
        initialized_dict[first_key]


def test_pop_too_many_arguments_error_message(initialized_dict):
    with pytest.raises(TypeError) as exception_info:
        initialized_dict.pop('key', 'default', 'extra')
    assert str(exception_info.value) == "pop expected at most 2 arguments, got 3"


def test_pop_key_error(memory_root, dict_type):
    d = initialize_dict(dict_type, {}, memory_root)
    with pytest.raises(KeyError):
        d.pop('not_a_key')


def test_pop_default(dict_type, memory_root):
    d = initialize_dict(dict_type, {}, memory_root)
    assert d.pop('not_a_key', 'default') == 'default'


def test_pop_too_many_arguments(memory_root, dict_type):
    d = initialize_dict(dict_type, {}, memory_root)
    with pytest.raises(TypeError):
        d.pop('not_a_key', 'default', 'extra')


# popitem()
def test_popitem(initialized_dict):
    last_item = list(initialized_dict.items())[-1]
    popped = initialized_dict.popitem()
    assert popped == last_item
    assert list(initialized_dict.items())[-1] != last_item

def test_popitem_empty(memory_root, dict_type):
    d = initialize_dict(dict_type, {}, memory_root)
    with pytest.raises(KeyError) as exeption_info:
        p = d.popitem()
    assert exeption_info.value.args[0] == 'popitem(): dictionary is empty'


# setdefault(key, default=None)
def test_setdefault(initialized_dict):
    existing_key = list(initialized_dict.keys())[0]
    assert initialized_dict.setdefault(existing_key, 'default value') == initialized_dict[existing_key]
    with pytest.raises(KeyError):
        # noinspection PyStatementEffect
        initialized_dict['z']
    assert initialized_dict.setdefault('z', 'default value') == 'default value'
    assert initialized_dict['z'] == 'default value'


def test_setdefault_error_typeerror_no_keyword_arguments(initialized_dict):
    with pytest.raises(TypeError):
        initialized_dict.setdefault('a', default='z')


# update()
def test_update_from_dict(initialized_dict):
    reference = initial_values.copy()
    update_dict = {'a': 2, 'z': 100}
    reference.update(update_dict)
    initialized_dict.update(update_dict)
    assert initialized_dict == reference


def test_update_from_dict_plus_additional_values(initialized_dict):
    reference = initial_values.copy()
    update_dict = {'a': 2, 'z': 100}
    additional_values = {'b': 4.4, 'y': 200}
    reference.update(update_dict, **additional_values)
    initialized_dict.update(update_dict, **additional_values)
    assert initialized_dict == reference


def test_update_from_list(initialized_dict):
    reference = initial_values.copy()
    update_list = [('a', 2), ('z', 100)]
    reference.update(update_list)
    initialized_dict.update(update_list)
    assert initialized_dict == reference


def test_update_from_list_plus_additional_values(initialized_dict):
    reference = initial_values.copy()
    update_list = [('a', 2), ('z', 100)]
    additional_values = {'b': 4.4, 'y': 200}
    reference.update(update_list, **additional_values)
    initialized_dict.update(update_list, **additional_values)
    assert initialized_dict == reference


def test_update_from_values_only(initialized_dict):
    reference = initial_values.copy()
    additional_values = {'b': 4.4, 'y': 200}
    reference.update(**additional_values)
    initialized_dict.update(**additional_values)
    assert initialized_dict == reference


# values()
def test_values(initialized_dict):
    source_values = list(initial_values.values())
    test_values_list = list(initialized_dict.values())
    assert len(test_values_list) == len(source_values)
    for test_value in test_values_list:
        assert test_value in source_values


# class methods
# fromkeys(iterable, value=None)
def test_fromkeys(dict_type):
    key_values = 'abcd'
    d = dict_type.fromkeys(key_values)
    assert d == {'a': None, 'b': None, 'c': None, 'd': None}
    d2 = dict_type.fromkeys(key_values, 400)
    assert d2 == {'a': 400, 'b': 400, 'c': 400, 'd': 400}


def test_fromkeys_error_typeerror_no_keyword_arguments(dict_type):
    with pytest.raises(TypeError):
        dict_type.fromkeys('abcd', value=400)


# d.__ge__(other); d >= other
def test_ge(initialized_dict):
    with pytest.raises(TypeError) as exception_info:
        initialized_dict >= initialized_dict
    assert "'>=' not supported between instances of" in str(exception_info.value)

# d.__gt__(other); d > other
def test_gt(initialized_dict):
    with pytest.raises(TypeError) as exception_info:
        initialized_dict > initialized_dict
    assert "'>' not supported between instances of" in str(exception_info.value)

# d.__le__(other); d <= other
def test_le(initialized_dict):
    with pytest.raises(TypeError) as exception_info:
        initialized_dict <= initialized_dict
    assert "'<=' not supported between instances of" in str(exception_info.value)

# d.__lt__(other); d >= other
def test_lt(initialized_dict):
    with pytest.raises(TypeError) as exception_info:
        initialized_dict < initialized_dict
    assert "'<' not supported between instances of" in str(exception_info.value)
