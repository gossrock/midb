import pytest
import random
from midb.backend._serialization import get_class_id
from midb.persistent_objects import PTuple, PList


# __init__
def test_init_default():
    t = PTuple()
    assert type(t) is PTuple
    assert type(t._temp) is tuple
    assert t.in_memory() == ()
    assert len(t) == 0
    assert t._backend is None
    assert t._id is None


def test_init_some_values():
    t = PTuple(['a', 'b', 'c'])
    assert type(t) is PTuple
    assert type(t._temp) is tuple
    assert t.in_memory() == ('a', 'b', 'c')
    assert len(t) == 3
    assert t._backend is None
    assert t._id is None


def test_init_with_backend(memory_backend):
    t = PTuple([1, 2, 3], _backend=memory_backend)
    memory_backend.root['test'] = t
    assert type(t) is PTuple
    assert t._temp is None
    assert t.in_memory() == (1, 2, 3)
    assert len(t) == 3


def test_init_backend_and_id(memory_backend):
    memory_backend.root['test'] = [1, 2, 3]
    _id = memory_backend.root['test']._id
    obj = PList(_backend=memory_backend, _id=_id)
    assert obj._temp is None
    assert obj.in_memory() == [1, 2, 3]
    assert obj._backend == memory_backend
    assert obj._id == _id


def test_init_ignore_initial_data(memory_root):
    memory_root['test'] = [1, 2, 3]
    _id = memory_root['test']._id
    obj = PList(['a', 'b', 'c'], _backend=memory_root._backend, _id=_id)
    assert obj._temp is None
    assert obj._backend == memory_root._backend
    assert obj._id == _id
    assert obj.in_memory() == [1, 2, 3]



# in_memory()
def test_in_memory_temp():
    t = PTuple(['a', 'b', 'c'])
    assert t._temp == ('a', 'b', 'c')
    assert t.in_memory() == ('a', 'b', 'c')
    assert t._temp is t.in_memory()


def test_in_memory_db(memory_root):
    memory_root['test'] = PTuple(['a', 'b', 'c'])
    t = memory_root['test']
    assert t._temp is None
    assert t.in_memory() == ('a', 'b', 'c')


# _move_temp_to_backend()
def test_move_temp_to_backend(memory_root):
    init = (False, 1, 3.0)
    t = PTuple(init)
    assert t._temp == init
    assert t._backend is None
    assert t._id is None
    memory_root['test'] = t
    assert t._temp is None
    assert t._backend == memory_root._backend
    assert t._id == 1

def test_move_temp_to_backend_no_temp(memory_backend):
    # unlikely situation. testing guarding if statement logic
    t = PTuple()
    t._temp = None
    _id = memory_backend.add_new_persistent_object(get_class_id(t))
    t._id = _id
    t._backend = memory_backend
    t._move_temp_to_backend() # this should keep mutmut from creating a mutation due to exception






# _not_reserved_setattr()
def test_not_reserved_setattr(memory_root):
    memory_root['test'] = PTuple(['a', 'b', 'c'])
    t = memory_root['test']

    with pytest.raises(AttributeError):
        t.not_reserved_attr


# _set_temp()
def test_set_temp_tuple():
    t = PTuple(['a', 'b', 'c'])
    with pytest.raises(TypeError):
        t._set_temp(0, 1)
    with pytest.raises(TypeError):
        t[0] = 1


def test_set_temp_list():
    l = PList(['a', 'b', 'c'])
    l._set_temp(0, 1)
    assert l == [1, 'b', 'c']
    l[1] = 2
    assert l == [1, 2, 'c']

def test_set_temp_out_of_range():
    l = PList(['a', 'b', 'c'])
    with pytest.raises(IndexError) as exception_info:
        l._set_temp(3, 1)
    assert exception_info.value.args[0] == f'PList assignment index out of range'


# _get_temp()
def test_get_temp():
    t = PTuple(['a', 'b', 'c'])
    assert t._get_temp(0) == 'a'
    assert t[0] == 'a'

# _del_temp()
def test_del_temp():
    t = PTuple(['a', 'b', 'c'])
    with pytest.raises(TypeError):
        t._del_temp(0)
    with pytest.raises(TypeError):
        del t[0]


# __get_item__(); t[key];
def test_get_item_int(memory_root):
    memory_root['test'] = ('a', 'b', 'c')
    assert memory_root['test'][0] == 'a'
    assert memory_root['test'][1] == 'b'
    assert memory_root['test'][2] == 'c'


def test_get_item_negetive_int(memory_root):
    memory_root['test'] = ('a', 'b', 'c')
    assert memory_root['test'][-3] == 'a'
    assert memory_root['test'][-2] == 'b'
    assert memory_root['test'][-1] == 'c'


def test_get_item_slice(memory_root):
    memory_root['test'] = ('a', 'b', 'c')
    assert memory_root['test'][1:3] == ('b', 'c')


def test_get_item_out_of_range(memory_root):
    memory_root['test'] = ('a', 'b', 'c')
    with pytest.raises(IndexError):
        memory_root['test'][3]
    with pytest.raises(IndexError):
        memory_root['test'][-4]


def test_get_item_not_int_or_slice(memory_root):
    memory_root['test'] = ('a', 'b', 'c')
    with pytest.raises(TypeError):
        memory_root['test']['a']


# __contains__; x in t
def test_contains_temp():
    t = PTuple(('a', 'b', 'c'))
    assert 'a' in t
    assert 'b' in t
    assert 'c' in t
    assert 'd' not in t


def test_contains_in_db(memory_root):
    t = PTuple(('a', 'b', 'c'))
    memory_root['test'] = t
    assert 'a' in t
    assert 'b' in t
    assert 'c' in t
    assert 'd' not in t

# __add__(), __radd__(); t + other, other + t
def test_add_radd(rand_tuple_1, rand_tuple_2):
    ptup1 = PTuple(rand_tuple_1)
    ptup2 = PTuple(rand_tuple_2)
    result = rand_tuple_1 + rand_tuple_2
    assert ptup1 + ptup2 == result
    assert ptup1 + rand_tuple_2 == result
    assert rand_tuple_1 + ptup2 == result

# __mul__(), __rmul__(); t * x, x * 3
def test_mul_rmul(rand_tuple_1):
    ptup = PTuple(rand_tuple_1)
    num = random.randint(3, 10)
    result = rand_tuple_1 * num
    assert ptup * num == result
    assert num * ptup == result


#
