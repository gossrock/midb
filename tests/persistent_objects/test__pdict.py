# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import pytest
from midb import get_root
from midb import get_backend, PDict, PTuple
from midb import PObject, MemoryObject, setup_pobject
from midb.backend import SQLiteBackend
from midb.backend._serialization import get_class_id


#PDict._equivalent_builtin_class
def test_equivalent_builtin_class():
    d = PDict()
    assert d._in_memory_class == dict

#PDict.__init__()
def test_init_default():
    d = PDict()
    assert type(d) is PDict
    assert type(d._temp) is dict
    assert d.in_memory() == {}
    assert len(d) == 0


def test_init_some_values():
    d = PDict({'a': 1, 'b': 2, 'c': 3})
    assert type(d) is PDict
    assert type(d._temp) is dict
    assert d.in_memory() == {'a': 1, 'b': 2, 'c': 3}
    assert len(d) == 3


def test_init_with_backend(memory_backend):
    d = PDict({'a': 1, 'b': 2, 'c': 3}, _backend=memory_backend)
    memory_backend.root['test'] = d
    assert type(d) is PDict
    assert d._temp is None
    assert d.in_memory() == {'a': 1, 'b': 2, 'c': 3}
    assert len(d) == 3

def test_create_PDict_persistent():
    backend = get_backend()
    backend.root['test'] = {'a':1, 2: 'b'}
    _id = backend.root['test']._id
    obj = PDict(_backend=backend, _id=_id)
    assert obj._backend is backend
    assert obj._id == 1
    assert obj._temp is None
    assert obj['a'] == 1
    assert obj[2] == 'b'

#PDict._move_temp_to_backend()
def test_move_temp_to_backend():
    root = get_root()
    temp = PDict({'test': 1})
    assert temp._backend is None
    assert temp._id is None
    assert temp._temp is not None
    root['not_temp'] = temp # will call _move_temp_to_backend()
    assert temp._backend == root._backend
    assert temp._id is not None
    assert temp._temp is None

def test_move_temp_to_backend_no_temp(memory_backend):
    # unlikely situation. testing guarding if statement logic
    d = PDict()
    d._temp = None
    _id = memory_backend.add_new_persistent_object(get_class_id(d))
    d._id = _id
    d._backend = memory_backend
    d._move_temp_to_backend() # this should keep mutmut from creating a mutation due to exception


# PDict.in_memory()
def test_in_memory_temp_not_set():
    d = PDict()
    d._temp = None
    assert d._temp == None
    with pytest.raises(ValueError) as exception_info:
        d.in_memory()
    assert exception_info.value.args[0] == "object is still temp but attribute '_temp' is not set."

    del d._temp
    assert not hasattr(d, "_temp")
    with pytest.raises(ValueError) as exception_info:
        d.in_memory()
    assert exception_info.value.args[0] == "object is still temp but attribute '_temp' is not set."


# PDict.items()
def test_items_with_persistent_objects():
    root = get_root()
    backend = root._backend
    assert type(backend) == SQLiteBackend
    root['test'] = {'a': 1}
    for _, value in root.items():
        assert value._backend == backend


# PDict.values()
def test_values_with_persistent_objects():
    root = get_root()
    backend = root._backend
    assert type(backend) == SQLiteBackend
    root['test'] = {'a': 1}
    for value in root.values():
        assert value._backend == backend

# hashable as key
def test_tuple_as_key(memory_root):
    memory_root[(1, 2)] = 'a'
    assert memory_root[(1, 2)] == 'a'
    memory_root[(1, 2)] = 'b'
    assert memory_root[(1, 2)] == 'b'
    memory_root[(1, 3)] = 'c'
    assert memory_root[(1, 3)] == 'c'
    assert memory_root[(1, 2)] == 'b'

def test_complex_tuple_as_key(memory_root):
    memory_root[(1, (2,))] = 'a'
    assert memory_root[(1, (2,))] == 'a'
    memory_root[(1, (2,))] = 'b'
    assert memory_root[(1, (2,))] == 'b'


def test_non_hashable_tuple(memory_root):
    with pytest.raises(TypeError):
        memory_root[(1, [2])] = 'a'
    with pytest.raises(TypeError):
        memory_root[(1, [2])]


def test_hashable_object(memory_root):
    class A(MemoryObject):
        def __init__(self, a):
            self.a = a

        def __hash__(self):
            return hash(self.a)

        def other_function(self):
            print('other function')

    @setup_pobject
    class PA(PObject):
        _in_memory_class = A

    pa1 = PA(1)
    assert hash(A(1)) == hash(1) == hash(pa1)

    memory_root[A(1)] = 1
    assert memory_root[A(1)] == 1
    memory_root[A(1)] = 2
    assert memory_root[A(1)] == 2
    assert memory_root[pa1] == 2
    assert len(memory_root.keys()) == 1
    for key in memory_root.keys():
        assert memory_root[key] == 2
    del memory_root[A(1)]
    with pytest.raises(KeyError):
        memory_root[A(1)]









