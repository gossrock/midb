# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
import random
from midb import get_root
from midb.persistent_objects import PTuple

initial_values = (1, 2.2, 'test', True)


class TempPTuple(PTuple):
    pass

tuple_types = [tuple, TempPTuple, PTuple]


def initialize_tuple(tuple_type, contents, root=None, root_key='test'):
    if tuple_type == PTuple:
        root[root_key] = tuple_type(contents)
        return root[root_key]
    else:
        return tuple_type(contents)


@pytest.fixture(params=tuple_types)
def tuple_type(request):
    yield request.param


@pytest.fixture(scope='function')
def initialized_tuple(tuple_type, memory_root):
    yield initialize_tuple(tuple_type, initial_values, memory_root)


# tuple(iterable=(), /)
def test_default_constructor(tuple_type):
    t = tuple_type()
    assert t == ()


def test_constructor_with_iterator(tuple_type):
    t = tuple_type(initial_values)
    assert t == initial_values


# t.__add__(value), t.__radd__(value) ; t + value, value + t
def test_add_radd(memory_root, tuple_type, rand_tuple_1, rand_tuple_2):
    test_tuple_a = initialize_tuple(tuple_type, rand_tuple_1, memory_root, root_key='key1')
    test_tuple_b = initialize_tuple(tuple_type, rand_tuple_2, memory_root,  root_key="key2")

    assert (rand_tuple_1 + rand_tuple_2) == (test_tuple_a + test_tuple_b) # __add__ tuple_type with tuple_type
    assert (rand_tuple_1 + rand_tuple_2) == (test_tuple_a + rand_tuple_2) # __add__ with value as tuple
    assert (rand_tuple_1 + rand_tuple_2) == (rand_tuple_1 + test_tuple_b) # __radd__ with value as tuple


# t.__contains__(key) ; key in t
def test_contains(initialized_tuple):
    assert initial_values[0] in initialized_tuple


# t. __eq__(value) ; t == value
def test_eq(initialized_tuple):
    assert initialized_tuple == initial_values


# t.__ge__(value) ; t >= value
def test_ge(memory_root, tuple_type, rand_tuple_1, rand_tuple_2):
    test_tup_a = initialize_tuple(tuple_type, rand_tuple_1, memory_root, 'key1')
    test_tup_b = initialize_tuple(tuple_type, rand_tuple_2, memory_root, 'key2')
    assert (rand_tuple_1 >= rand_tuple_2) == (test_tup_a >= test_tup_b)


# t.__getattribute__(name) ; getattr(t, name) ; t.name
def test_getattr(initialized_tuple):
    assert getattr(initialized_tuple, '__len__')() == len(initial_values)


# t.__getitem__(key) ; t[key]
def test_getitem(initialized_tuple):
    for i in range(len(initialized_tuple)):
        assert initialized_tuple[i] == initial_values[i]


# t.__gt__(value) ; t > value
def test_gt(memory_root, tuple_type, rand_tuple_1, rand_tuple_2):
    test_tup_a = initialize_tuple(tuple_type, rand_tuple_1, memory_root, 'key1')
    test_tup_b = initialize_tuple(tuple_type, rand_tuple_2, memory_root, 'key2')
    assert (rand_tuple_1 > rand_tuple_2) == (test_tup_a > test_tup_b)


# t.__hash__() ; hash(t)
def test_hash(initialized_tuple):
    assert hash(initialized_tuple) == hash(initial_values)


# t.__iter__() ; iter(t)
def test_iter(initialized_tuple):
    for a, b in zip(iter(initialized_tuple), iter(initial_values)):
        assert a == b


# t.__le__(value) ; t <= value
def test_le(memory_root, tuple_type, rand_tuple_1, rand_tuple_2):
    test_tup_a = initialize_tuple(tuple_type, rand_tuple_1, memory_root, 'key1')
    test_tup_b = initialize_tuple(tuple_type, rand_tuple_2, memory_root, 'key2')
    assert (rand_tuple_1 <= rand_tuple_2) == (test_tup_a <= test_tup_b)


# t.__len__() ; len(t)
def test_len(initialized_tuple):
    assert len(initialized_tuple) == len(initial_values)


# t.__lt__(value) ; t < value
def test_lt(memory_root, tuple_type, rand_tuple_1, rand_tuple_2):
    test_tup_a = initialize_tuple(tuple_type, rand_tuple_1, memory_root, 'key1')
    test_tup_b = initialize_tuple(tuple_type, rand_tuple_2, memory_root, 'key2')
    assert (rand_tuple_1 < rand_tuple_2) == (test_tup_a < test_tup_b)


# t.__mul__(value) ; t * value
def test_mul(initialized_tuple):
    for m in range(10):
        assert initialized_tuple * m == initial_values * m


# t.__ne__(value) ; t != value
def test_ne(initialized_tuple):
    assert initialized_tuple != initial_values[0:-1]


# t.__rmul__(value) ; value * t
def test_rmul(initialized_tuple):
    for m in range(10):
        assert m * initialized_tuple == m * initial_values


# t.count(value) ; # Return number of occurrences of value
def test_count(memory_root, tuple_type):
    a_count = random.randint(0,10)
    b_count = random.randint(0,10)
    initial_contents = (('a',) * a_count) + (('b',) * b_count)
    t = initialize_tuple(tuple_type, initial_contents, memory_root)
    assert t.count('a') == a_count
    assert t.count('b') == b_count


# t.index(value, start=0, stop=9223372036854775807)
    # Return first index of value. Raises ValueError if the value is not present.
def test_index(memory_root, tuple_type):
    contents = list('0123456789')
    test_index = random.randint(0,10)
    test_value = 'a'
    contents.insert(test_index, test_value)
    t = initialize_tuple(tuple_type, contents, memory_root)
    assert t.index(test_value) == test_index
