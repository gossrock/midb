# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from midb import get_backend
from midb.persistent_objects import PList
from midb.backend import SQLiteBackend

def new_temp_plist():
    return PList()

def new_persistent_plist():
    backend = get_backend(root_type=PList)
    return backend.root

TEST_OBJ_FUNCTIONS = [new_temp_plist, new_persistent_plist]

@pytest.fixture(scope="function", params=TEST_OBJ_FUNCTIONS)
def obj(request):
    return request.param()

def test_create_PList_temp():
    obj = PList([1, 2])
    assert obj._backend is None
    assert obj._id is None
    assert type(obj._temp) is list
    assert obj._temp[0] == 1
    assert obj._temp[1] == 2
    assert obj[0] == 1
    assert obj[1] == 2

def test_create_PList_persistent():
    backend = get_backend()
    backend.root['test'] = [1, 2]
    _id = backend.root['test']._id
    obj = PList(_backend=backend, _id=_id)
    assert obj._backend is backend
    assert obj._id == 1
    assert obj._temp is None
    assert obj[0] == 1
    assert obj[1] == 2

def test_create_PList_temp_turn_persistent():
    obj = PList([1, 2])
    assert obj._backend is None
    assert obj._id is None
    assert type(obj._temp) is list
    root = SQLiteBackend().root
    root['obj'] = obj
    assert obj._backend == root._backend
    assert obj._id is not None
    assert obj._temp is None
    assert obj[0] == 1
    assert obj[1] == 2
    assert root['obj'][0] == 1
    assert root['obj'][1] == 2


def test_PList_append_get_set_del_int(obj, value_list):
    for v in value_list:
        assert v not in obj
        obj.append(v)
        assert obj[0] == v
        assert obj == [v]
        assert v in obj
        del obj[0]
    with pytest.raises(IndexError):
        obj[0]
    assert obj == []
    for v in value_list:
        obj.append(v)
    for v1, v2 in zip(value_list, obj):
        assert v1 == v2
    assert obj == value_list
    value_list.insert(5, 'a')
    obj.insert(5, 'a')
    del obj[:]
    assert obj == []

def test_set_get_del_neg_index(obj, value_list):
    for v in value_list:
        obj.append(v)
    for i in range(-1, -(len(obj) + 1), -1):
        assert i < 0
        assert obj[i] == value_list[i]
        obj[i] = 1
        assert obj[i] == 1

    for i in range(-len(obj)+1, 1):
        del obj[i]
    assert obj == []



    del obj[:]
    assert obj == []

def test_set_get_del_continuous_slice(obj):
    l = [x for x in range(10)]
    for v in l:
        obj.append(v)
    assert obj == l
    assert obj[3:6] == l[3:6]
    assert obj[-5:-1] == l[-5:-1]
    l[4:8] = ['a', 'b', 'c']
    obj[4:8] = ['a', 'b', 'c']
    assert obj == l
    del l[2:5]
    del obj[2:5]
    assert obj == l
    del obj[:]
    assert obj == []




def test_set_get_del_extended_slice(obj):
    l = [x for x in range(10)]
    for v in l:
        obj.append(v)


def test_PList_set_item_key_incorrect_type():
    obj = PList([1, 2])
    with pytest.raises(TypeError):
        obj['a'] = 1

    backend = SQLiteBackend()
    obj = PList([1, 2], _backend=backend, _id=0)
    with pytest.raises(TypeError):
        obj['a'] = 1

    with pytest.raises(TypeError):
        del obj['a']


