# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from datetime import date, time, datetime
from midb.backend._serialization import get_class_id, serialize, deserialize, SerializationError, DeserializationError

values = [None,
          "test", # strs
          1, -1, 0, 100, # ints
          1.1, -1.1, 0.0, # floats
          True, False, # bools
          datetime.now(), # datetime
          datetime.now().date(), # date
          datetime.now().time(), # time
         ]

value_tests_ids = [f'{get_class_id(v)}{v}' for v in values]


@pytest.mark.parametrize("value", values, ids=value_tests_ids)
def test_round_trip(value):
    assert deserialize(*serialize(value)) == value


def test_serialize_no_serializer():
    class NoSerializerClass:
        pass
    with pytest.raises(SerializationError):
        serialize(NoSerializerClass())


def test_deserialize_no_deserializer():
    class NoDeserializerClass:
        pass
    class_id = get_class_id(NoDeserializerClass)

    with pytest.raises(DeserializationError):
        deserialize(class_id, NoDeserializerClass())

def test_deserialize_incorrect_representations():
    with pytest.raises(ValueError) as e:
        deserialize(get_class_id(None), 'none')
    with pytest.raises(ValueError) as e:
        deserialize(get_class_id(bool), 'false')