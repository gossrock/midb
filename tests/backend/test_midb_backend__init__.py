import pytest
from midb.persistent_objects import BasePersistentObject, PDict, MemoryObject, PObject, setup_pobject, PTuple
from midb.backend import SQLiteBackend, run_sql
from midb.persistent_objects._basepersistentobject import convert_if_necessary
from midb.backend._serialization import get_class_id


def test_get_set_reset_delete_round_trip(key, value):
    backend = SQLiteBackend()
    parent_id = 0

    converted_key = convert_if_necessary(key)
    converted_value = convert_if_necessary(value)
    # not set check
    with pytest.raises(KeyError):
        backend.get(parent_id, converted_key)
    # set
    backend.set(parent_id, converted_key, converted_value)
    # get
    after_value = backend.get(parent_id, converted_key)
    assert after_value == value
    if isinstance(after_value, BasePersistentObject):
        assert type(after_value.in_memory()) == type(value)
    else:
        assert type(after_value) is type(value)
    # reset
    backend.set(parent_id, converted_key, 0)
    # re-get
    after_value = backend.get(parent_id, converted_key)
    assert after_value == 0
    assert type(after_value) is type(0)
    # delete
    backend.delete(parent_id, converted_key)
    # not set check
    with pytest.raises(KeyError):
        backend.get(parent_id, converted_key)


def test_get_keys_get_values_get_items(key_list, value_list):
    backend = SQLiteBackend()
    parent = 0
    key_list = [convert_if_necessary(key) for key in key_list]
    value_list = [convert_if_necessary(value) for value in value_list]

    for key, value in zip(key_list, value_list):
        backend.set(parent, key, value)
    assert backend.get_keys(parent) == key_list
    assert backend.get_values(parent) == value_list
    assert backend.get_items(parent) == list(zip(key_list, value_list))


# SQLiteBackend.__init__()
def test_SQLiteBackend_auto_commit_default_true(backend1, backend2):
    class MyClass:
        pass
    new_id = backend1.add_new_persistent_object(MyClass)
    backend1.set(new_id, 'test', 123)
    assert backend1.get(new_id, 'test') == backend2.get(new_id, 'test') == 123


def test_SQLiteBackend_auto_commit_false(backend1_auto_commit_off, backend2):
    class MyClass:
        pass
    backend1 = backend1_auto_commit_off
    new_id = backend1.add_new_persistent_object(MyClass)
    backend1.set(new_id, 'test', 123)
    assert backend1.get(new_id, 'test') == 123
    with pytest.raises(KeyError):
        backend2.get(new_id, 'test')
    backend1.commit()
    assert backend2.get(new_id, 'test') == 123


# SQLiteBackend.root
def test_root():
    backend = SQLiteBackend(root_class=PDict)
    assert isinstance(backend.root, BasePersistentObject)
    assert type(backend.root) == PDict
    assert backend.root._id == 0

# SQLiteBackend.__repr__
def test_repr(file_backend):
    backend = file_backend
    assert repr(backend) == 'SQLiteBackend(filename="test.db")'

# SQLiteBackend.__str__
def test_str(file_backend):
    backend = file_backend
    assert str(backend) == 'SQLiteBackend(filename="test.db")'


# SQLiteBackend._get_equivalent_key_or_same()
def test_get_equivalent_key_or_same_nothing_already_set(memory_backend):
    parent_id = memory_backend.root._id
    key = 'test'
    assert memory_backend._get_equivalent_key_or_same(parent_id, key) == key


def test_get_equivalent_key_or_same_int_set_bool_key_lookup(memory_backend):
    memory_backend.root[1] = 1
    parent_id = memory_backend.root._id
    key = True
    assert memory_backend._get_equivalent_key_or_same(parent_id, key) == key
    assert type(memory_backend._get_equivalent_key_or_same(parent_id, key)) == int


def test_get_equivalent_key_or_same_custom_object(memory_backend):
    class A(MemoryObject):
        def __init__(self, a, b):
            self.a = a
            self.b = b

        def __hash__(self):
            return hash(self.a)

        def __eq__(self, other):
            return other.a == self.a

    @setup_pobject
    class PA(PObject):
        _in_memory_class = A

    a1 = A(1, 1)
    a2 = A(1, 2)
    memory_backend.root[a1] = 1
    parent_id = memory_backend.root._id
    key = a2
    ret_key = memory_backend._get_equivalent_key_or_same(parent_id, key)
    assert  ret_key == key == a1 == a2
    assert type(memory_backend._get_equivalent_key_or_same(parent_id, key)) == PA
    assert ret_key.b == 1


#SQLiteBackend._delete_persistent_object()
def test_delete_persistent_object_utility_function(memory_root):
    initial_data = {'a': 1, 'b': 2}
    memory_root['dict'] = initial_data
    persistent_object = memory_root['dict']
    assert persistent_object._temp is None
    assert type(persistent_object._id) is int
    assert persistent_object._backend == memory_root._backend

    memory_root._backend._delete_persistent_object(persistent_object)

    assert persistent_object._temp == initial_data
    assert persistent_object._id is None
    assert persistent_object._backend is None


# SQLiteBackend.get()
def test_get_unhashable_type(memory_backend):
    with pytest.raises(TypeError) as exeption_info:
        memory_backend.get(parent_id=0, key=[0])
    assert str(exeption_info.value) == "unhashable type: 'list'"


# SQLiteBackend.set()
def test_set_update_persistent_object_value(memory_backend):
    r = memory_backend.root
    first_value = PTuple(('test1',))
    second_value = PTuple(('test2',))
    r['test'] = first_value
    assert memory_backend._number_of_records_that_a_persistent_object_is_in(first_value) == 1
    r['test'] = second_value
    assert memory_backend._number_of_records_that_a_persistent_object_is_in(first_value) == 0
    _, results = run_sql(memory_backend.connection, "SELECT * FROM persistent_objects where id = ?", [first_value._id])
    assert len(results) == 0

def test_set_unhashable_type(memory_backend):
    with pytest.raises(TypeError) as exeption_info:
        memory_backend.set(parent_id=0, key=[0], value=1)
    assert str(exeption_info.value) == "unhashable type: 'list'"


def test_set_key_with_different_type_equivalent_value(memory_backend):
    memory_backend.set(parent_id=0, key=1, value='a')
    assert memory_backend.get(parent_id=0, key=1) == 'a'
    saved_key = list(memory_backend.get_keys(parent_id=0))[0]
    assert type(saved_key) is int

    memory_backend.set(parent_id=0, key=True, value='b')

    saved_key = list(memory_backend.get_keys(parent_id=0))[0]
    assert type(saved_key) is int
    assert memory_backend.get(parent_id=0, key=True) == 'b'
    assert memory_backend.get(parent_id=0, key=1) == 'b'


#SQLiteBackend.add_new_persistent_object()
def test_add_new_persistent_object(backend1, backend2):
    class MyClass:
        pass
    class_id = get_class_id(MyClass)
    new_id = backend1.add_new_persistent_object(MyClass)
    assert new_id == 1
    _, results = run_sql(backend1.connection, "SELECT * FROM persistent_objects WHERE id=?", [new_id])
    assert results[0][0] == new_id
    assert results[0][1] == class_id
    # make sure that it was committed
    _, results = run_sql(backend2.connection, "SELECT * FROM persistent_objects WHERE id=?", [new_id])
    assert results[0][0] == new_id
    assert results[0][1] == class_id


# SQLiteBackend.delete()
def test_delete_commit_default_true(backend1, backend2):
    contents = {'test1': 'A', 'test2': 'B', 'test3': 'C'}
    class_id = 'my_class'
    new_id = backend1.add_new_persistent_object(class_id)
    for key, value in contents.items():
        backend1.set(new_id, key, value)
    for key, value in contents.items():
        assert backend1.get(new_id, key) == backend2.get(new_id, key) == value

    backend1.delete(new_id, 'test2')

    with pytest.raises(KeyError):
        assert backend1.get(new_id, 'test2')
    with pytest.raises(KeyError):
        assert backend2.get(new_id, 'test2')


def test_delete_persistent_object_value_that_is_also_a_key(memory_backend):
    memory_backend.root[(1, 2)] = 'test'
    key_value = list(memory_backend.get_keys(parent_id=0))[0]
    memory_backend.root['test'] = key_value
    assert memory_backend.root[(1, 2)] == 'test'
    assert memory_backend.root['test'] == (1, 2)

    memory_backend.delete(parent_id=0, key='test')

    assert memory_backend.root[(1, 2)] == 'test'
    with pytest.raises(KeyError):
        memory_backend.root['test']


def test_delete_persistent_object_key_value_complete_deletion(memory_backend):
    t = PTuple((1, 2))
    memory_backend.root[t] = 'test'
    assert type(t._id) is int
    t_id = t._id
    _, results = run_sql(connection=memory_backend.connection,
                         sql="SELECT * from persistent_objects where id=?",
                         sql_values=[t_id])
    assert len(results) == 1

    memory_backend.delete(parent_id=memory_backend.root._id, key=t)

    _, results = run_sql(connection=memory_backend.connection,
                         sql="SELECT * from persistent_objects where id=?",
                         sql_values=[t_id])
    assert len(results) == 0


def test_delete_persistent_object_key_used_multiple_times(memory_backend):
    t = PTuple((1, 2))
    d1 = PDict({t: 'a'})
    d2 = PDict({t: 'a'})
    memory_backend.root['test1'] = d1
    memory_backend.root['test2'] = d2

    memory_backend.delete(parent_id=d1._id, key=t)

    _, results = run_sql(connection=memory_backend.connection,
                         sql="SELECT * from persistent_objects where id=?",
                         sql_values=[t._id])
    assert len(results) == 1


def test_delete_persestent_object_both_key_and_value_of_record(memory_backend):
    t = PTuple((1, 2))
    memory_backend.root[t] = t
    assert memory_backend.root[t] == t
    t_id = t._id

    _, results = run_sql(connection=memory_backend.connection,
                         sql="SELECT * from persistent_objects where id=?",
                         sql_values=[t_id])
    assert len(results) == 1

    memory_backend.delete(parent_id=memory_backend.root._id, key=t)

    _, results = run_sql(connection=memory_backend.connection,
                         sql="SELECT * from persistent_objects where id=?",
                         sql_values=[t_id])
    assert len(results) == 0


def test_delete_persistent_object():
    backend = SQLiteBackend()
    root = backend.root
    root['test_a'] = PDict({'test_b': 1})
    sub_id = root['test_a']._id
    assert type(root['test_a']) == PDict
    assert root['test_a']['test_b'] == 1
    backend.delete(root._id, 'test_a')
    with pytest.raises(KeyError):
        backend.get(sub_id, 'test_b')
    _, results = run_sql(backend.connection, "Select * from persistent_objects Where id=?", [sub_id])
    assert len(results) == 0


def test_delete_all_children(backend1, backend2):
    contents = {'test1': 'A', 'test2': 'B', 'test3': 'C'}
    class_id = 'my_class'
    new_id = backend1.add_new_persistent_object(class_id)
    for key, value in contents.items():
        backend1.set(new_id, key, value)
    for key, value in contents.items():
        assert backend1.get(new_id, key) == backend2.get(new_id, key) == value

    backend1.delete_all_children(new_id)

    for key in contents.keys():
        with pytest.raises(KeyError):
            backend1.get(new_id, key)
        with pytest.raises(KeyError):
            backend2.get(new_id, key)


def test_delete_all_children_commit_false(backend1, backend2):
    # setup
    contents = {'test1': 'A', 'test2': 'B', 'test3': 'C'}
    class_id = 'my_class'
    new_id = backend1.add_new_persistent_object(class_id)
    for key, value in contents.items():
        backend1.set(new_id, key, value)
    for key, value in contents.items():
        assert backend1.get(new_id, key) == backend2.get(new_id, key) == value

    # action to test
    backend1.delete_all_children(new_id, commit=False)

    # check
    for key, value in contents.items():
        with pytest.raises(KeyError):
            backend1.get(new_id, key)
        assert backend2.get(new_id, key) == value


# list_shift()
def test_list_shift(backend1, backend2):
    list_id = 0
    values = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    for index, value in enumerate(values):
        backend1.set(list_id, index, value)
    backend1.list_shift(list_id, 3, 2)
    assert backend1.get_keys(list_id) == backend2.get_keys(list_id) == [0, 1, 2, 5, 6, 7, 8]


def test_list_del_item(backend1, backend2):
    list_id = 0
    values = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    for index, value in enumerate(values):
        backend1.set(list_id, index, value)
    backend1.list_del_item(list_id, 3)
    del(values[3])
    assert len(backend1.get_values(list_id)) == len(backend2.get_values(list_id)) == len(values)
    for i in range(len(values)):
        assert backend1.get(list_id, i) == backend2.get(list_id, i) == values[i]


def test_list_insert(backend1, backend2):
    list_id = 0
    values = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    for index, value in enumerate(values):
        backend1.set(list_id, index, value)
    backend1.list_insert(list_id, 3, 'A')
    values.insert(3, 'A')
    assert len(backend1.get_values(list_id)) == len(backend2.get_values(list_id)) ==  len(values)
    for i in range(len(values)):
        assert backend1.get(list_id, i) == backend2.get(list_id, i) == values[i]



def test_list_insert_invalid_index_type():
    backend = SQLiteBackend()
    list_id = 0
    values = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    for index, value in enumerate(values):
        backend.set(list_id, index, value)
    with pytest.raises(TypeError):
        backend.list_insert(list_id, '3', 'A')


def test_list_del_multiple(backend1, backend2):
    list_id = 0
    values = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    for index, value in enumerate(values):
        backend1.set(list_id, index, value)
    backend1.list_del_multiple(list_id, [3, 5])
    del(values[5])
    del(values[3])
    assert len(backend1.get_values(list_id)) == len(values)
    assert len(backend2.get_values(list_id)) == len(values)
    for i in range(len(values)):
        assert backend1.get(list_id, i) == backend2.get(list_id, i) == values[i]
