# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
import os
import sqlite3
import midb.backend._db_creation_and_validation as dbcv

MEMORYDB = ":memory:"


def test__run_sql_new_id():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER, type TEXT)")
    id, _ = dbcv.run_sql(con, "INSERT INTO persistent_objects DEFAULT VALUES")
    assert id == 1


def test_persistent_objects_table_exists__does_not_exist():
    con = sqlite3.connect(MEMORYDB)
    assert dbcv._persistent_objects_table_exists(connection=con) is False


def test_persistent_objects_table_exists__exists():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER)")
    assert dbcv._persistent_objects_table_exists(connection=con) is True


def test_persistent_objects_table_valid__not_valid():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER)")
    assert dbcv._persistent_objects_table_valid(connection=con) is False


def test_persistent_objects_table_valid__valid():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    assert dbcv._persistent_objects_table_valid(connection=con) is True


def test_persistent_objects_table_has_root_record__does_not_have():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_object (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    assert dbcv._persistent_objects_table_has_root_record(connection=con) is False


def test_persistent_objects_table_has_root_record__has():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    values = ["midb.persistent_objects.PKVStore"]
    con.execute("INSERT INTO persistent_objects VALUES (0, ?)", values)
    assert dbcv._persistent_objects_table_has_root_record(connection=con) is True


def test_key_value_table_exists__does_not_exist():
    con = sqlite3.connect(MEMORYDB)
    assert dbcv._key_value_table_exists(connection=con) is False


def test_key_value_table_exists__exists():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE key_value (id INTEGER)")
    assert dbcv._key_value_table_exists(connection=con) is True


def test_key_value_valid__not_valid():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE key_value (id INTEGER)")
    assert dbcv._key_value_table_valid(connection=con) is False


def test_dict_key_value_valid__valid():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE IF NOT EXISTS key_value ("
                            "parent_id INTEGER, "
                            "key_type TEXT, "
                            "key TEXT, "
                            "value_type TEXT, "
                            "value TEXT)")
    assert dbcv._key_value_table_valid(connection=con) is True


def test_valid_db__valid():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    values = ["midb.persistent_objects.PKVStore"]
    con.execute("INSERT INTO persistent_objects VALUES (0, ?)", values)
    con.execute("CREATE TABLE IF NOT EXISTS key_value ("
                            "parent_id INTEGER, "
                            "key_type TEXT, "
                            "key TEXT, "
                            "value_type TEXT, "
                            "value TEXT)")
    assert dbcv.valid_db(connection=con) is True


# SqliteAdapter.valid_db()
def test_valid_db__not_valid__no_root_record():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    #con.execute("INSERT INTO persistent_objects VALUES (0)")
    con.execute("CREATE TABLE IF NOT EXISTS key_value ("
                            "parent_id INTEGER, "
                            "key_type TEXT, "
                            "key TEXT, "
                            "value_type TEXT, "
                            "value TEXT)")
    assert dbcv.valid_db(connection=con) is False


def test_valid_db__not_valid__no_persistent_objects_table():
    con = sqlite3.connect(MEMORYDB)
    #con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY)")
    # con.execute("INSERT INTO persistent_objects VALUES (0)")
    con.execute("CREATE TABLE IF NOT EXISTS key_value ("
                            "parent_id INTEGER, "
                            "key_type TEXT, "
                            "key TEXT, "
                            "value_type TEXT, "
                            "value TEXT)")
    assert dbcv.valid_db(connection=con) is False


def test_valid_db__not_valid__no_key_value_table():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    values = ["midb.persistent_objects.PKVStore"]
    con.execute("INSERT INTO persistent_objects VALUES (0, ?)", values)
    #con.execute("CREATE TABLE key_value (dictid INTEGER,key TEXT, key_type TEXT, value TEXT, value_type TEXT)")
    assert dbcv.valid_db(connection=con) is False


def test_valid_db__not_valid__invalid_persistent_objects_table():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER)")
    con.execute("INSERT INTO persistent_objects VALUES (0)")
    con.execute("CREATE TABLE key_value (dictid INTEGER,key TEXT, key_type TEXT, value TEXT, value_type TEXT)")
    with pytest.raises(ValueError) as exception_info:
        dbcv.valid_db(connection=con)
    assert exception_info.value.args[0] == "incompatible database."


def test_valid_db__not_valid__invalid_key_value_table():
    con = sqlite3.connect(MEMORYDB)
    con.execute("CREATE TABLE persistent_objects (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)")
    values = ["midb.persistent_objects.PKVStore"]
    con.execute("INSERT INTO persistent_objects VALUES (0, ?)", values)
    con.execute("CREATE TABLE key_value (dictid INTEGER)")
    with pytest.raises(ValueError) as exception_info:
        dbcv.valid_db(connection=con)
    assert exception_info.value.args[0] == "incompatible database."


def test_create_db(db_file):
    dbcv.create_db(db_file)
    con = sqlite3.connect(db_file)
    assert dbcv.valid_db(con) is True
