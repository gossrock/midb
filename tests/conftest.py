# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
import os
import random
import midb
import midb.backend
from datetime import datetime


class CustomObject(midb.MemoryObject):
    def __init__(self, a):
        self.a = a

    def __hash__(self):
        return hash(self.a)


@midb.setup_pobject
class PCustomObject(midb.PObject):
    _in_memory_class = CustomObject

# the number of keys must be the same as the number of values for some tests. String keys are used for value elements that
# are not hashable or equivelent to some other value that has already been used.
keys = [None,
          "test", # strs
          True, False, # bools
          "0", "1", -1, 10, 100, # int  (None: 1 is equivalent to True in Python dictionary keys and 0 is equivalent to False)
          "1.0", "0.0", 1.1, -1.1, 0.01, # floats (Note: floats equal to ints are equivalent in Python dictionary keys)
          "0+0j", "1+0j", 2+1j, -3+2j, -4-3j, 5.4+2.3j, # complex (Note: complex equal to floats are equivalent as dict keys)
          datetime.now(), # datetime
          datetime.now().date(), # date
          datetime.now().time(), # time
          (1, 2),
          CustomObject('custom object'),
          'dict key',
          'list key',
         ]

values = [None,
          "test", # strs
          True, False, # bools
          0, 1, -1, 10, 100, # ints
          1.0, 0.0, 1.1, -1.1, 0.01, # floats
          0+0j, 1+0j, 2+1j, -3+2j, -4-3j, 5.4+2.3j, # complex
          datetime.now(), # datetime
          datetime.now().date(), # date
          datetime.now().time(), # time
          (1, 2),
          CustomObject('custom object'),
          {'a': 1, 'b': 2},
          [1, 2, 3],
         ]


@pytest.fixture(scope='function')
def memory_backend():
    yield midb.get_backend()

@pytest.fixture(scope='function')
def memory_backend2():
    yield midb.get_backend()

def try_delete(file_name):
    try:
        os.remove(file_name)
    except FileNotFoundError:
        pass


@pytest.fixture(scope='function')
def db_file():
    db_file_name = "test.db"
    yield db_file_name
    try_delete(db_file_name)
    try_delete(db_file_name+"-shm")
    try_delete(db_file_name+"-wal")


@pytest.fixture(scope='function')
def file_backend(db_file):
    yield midb.get_backend('test.db')


@pytest.fixture(scope='function')
def backend1(db_file):
    yield midb.backend.SQLiteBackend('test.db')


@pytest.fixture(scope='function')
def backend1_auto_commit_off(db_file):
    yield midb.backend.SQLiteBackend('test.db', auto_commit=False)


@pytest.fixture(scope='function')
def backend2(db_file):
    yield midb.get_backend('test.db')


@pytest.fixture(scope='function')
def memory_root(memory_backend):
    yield memory_backend.root

@pytest.fixture(scope='function')
def memory_root2(memory_backend2):
    yield memory_backend2.root


@pytest.fixture(scope='function')
def file_root(file_backend):
    yield file_backend.root


@pytest.fixture(scope="function", params=keys)
def key(request):
    return request.param


@pytest.fixture(scope="function")
def key_list():
    return keys


@pytest.fixture(scope="function", params=values)
def value(request):
    return request.param


@pytest.fixture(scope="function")
def value_list():
    return values


def random_tuple():
    return tuple([random.choice('abcde') for _ in range(5)])


@pytest.fixture(scope='function')
def rand_tuple_1():
    return random_tuple()


@pytest.fixture(scope='function')
def rand_tuple_2():
    return random_tuple()
