import os
import pytest
import midb
from midb.backend import SQLiteBackend


# get_root
def test_get_root_default():
    root = midb.get_root()
    assert type(root) == midb.DEFAULT_ROOT_CLASS


# get_backend
def test_get_backend_default():
    backend = midb.get_backend()
    assert type(backend) == SQLiteBackend



