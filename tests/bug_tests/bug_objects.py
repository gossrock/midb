import midb

class HashObject(midb.MemoryObject):
    def __init__(self, value):
        self.value = value

    def __hash__(self):
        return hash(self.value)


@midb.setup_pobject
class PHashObject(midb.PObject):
    _in_memory_class = HashObject