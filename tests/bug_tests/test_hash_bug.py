from typing import NamedTuple
import subprocess

# COPIED FROM devtools/simplistic_scripting.py
# TODO: remove in favor of using the same file (separate project or some way of including that file in the path)
class Output(NamedTuple):
    stdout: str
    stderr: str
    return_code: int


def run(command: str, stdin: str = "", display=False):
    if display:
        print(command)
    process = subprocess.Popen(command, encoding='utf-8', stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               shell=True, executable='/bin/bash')
    stdout, stderr = process.communicate(stdin)
    if display and stdout.strip() != "":
        print(stdout)
    if display and stderr.strip() != "":
        print(stderr)
    return_code = process.wait()
    return Output(stdout, stderr, return_code)



# TESTS
import midb
import os
import bug_objects


def test_hash_after_restart():
    db_file = 'hash_bug_test.py'
    try:
        os.remove(db_file)
    except FileNotFoundError:
        pass
    DB = midb.get_root(db_file)
    std_out, std_err, return_code = run('python3 tests/bug_tests/set_value.py')
    assert return_code == 0
    assert DB[bug_objects.HashObject('test')] == 'test'
    os.remove(db_file)




