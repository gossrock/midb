import midb
from my_objects import MyObj, PMyObj

root = midb.get_root("filename.db")

root['my object'] = MyObj(1, 2)
print(root['my object'].a)
print(root['my object'].b)
print(root['my object'].some_other_method(3))
print(root['my object'].methods_may_set)
