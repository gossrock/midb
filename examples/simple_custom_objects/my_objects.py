import midb

# define or import an class
# Note: subclassing midb.MemoryObject is not necessary but useful since it defines __repr__ and __eq__.
class MyObj(midb.MemoryObject):
    def __init__(self, a_value, b_value):
        self.a = a_value
        self.b = b_value

    def some_other_method(self, input):
        self.methods_may_set = input
        return f"{self.a}, {self.b}"

# define a persistent object with an _in_memory_class attribute set to the object you
# want to have it emulate and decorate it with the @midb.setup_pobject decorator.
@midb.setup_pobject
class PMyObj(midb.PObject):
    _in_memory_class = MyObj