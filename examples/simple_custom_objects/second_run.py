import midb
from my_objects import MyObj, PMyObj

root = midb.get_root("filename.db")

print(root['my object'].a)
print(root['my object'].b)
print(root['my object'].methods_may_set)
print(root['my object'].some_other_method(4))
print(root['my object'].methods_may_set)
