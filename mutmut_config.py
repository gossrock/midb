def pre_mutation(context):
    IGNORE_LINES_WITH = ['@abstractmethod', '@staticmethod', "@property", "commit=False"]
    line = context.current_source_line.strip()
    for ignore_part in IGNORE_LINES_WITH:
        if ignore_part in line:
            context.skip = True