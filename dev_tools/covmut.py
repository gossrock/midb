#! ../.venv/bin/python
import argparse
from simplistic_scripting import run
from build import remove_previous_build, build_and_install

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='run coverage and mutmut on project files.')
    parser.add_argument('file_filter', type=str, nargs='*',
                        help='path or portion of path to file to check.')
    args = parser.parse_args()
    print(f'{"-" * 80}')
    print(f'{"source file":^50s}{"coverage":^15s}{"mutants":^15s}')
    print(f'{"-" * 80}')

    module_info = {
        'midb/__init__.py': "tests/test_midb__init__.py",
        'midb/backend/__init__.py': "tests/backend/test_midb_backend__init__.py",
        'midb/backend/_db_creation_and_validation.py': 'tests/backend/test__db_creation_and_validation.py',
        'midb/backend/_serialization.py': 'tests/backend/test__serialization.py',
        'midb/persistent_objects/_basepersistentobject.py': "tests/persistent_objects/test__basepersistentobject.py",
        'midb/persistent_objects/_persistentsequence.py': "tests/persistent_objects/test__persistentsequence.py",
        'midb/persistent_objects/_pdict.py': "tests/persistent_objects/test_dict_interface.py tests/persistent_objects/test__pdict.py",
        'midb/persistent_objects/_ptuple.py': 'tests/persistent_objects/test_tuple_interface.py tests/persistent_objects/test__ptuple.py',
        'midb/persistent_objects/_plist.py': 'tests/persistent_objects/test_list_interface.py tests/persistent_objects/test__plist.py',
        'midb/persistent_objects/_pobject.py': 'tests/persistent_objects/test_object_interface.py tests/persistent_objects/test__pobject.py',
    }
    files_to_test = {}
    if len(args.file_filter) > 0:
        for filter in args.file_filter:
            files_to_test.update({file: test for file, test in module_info.items() if filter in file})
    else:
        files_to_test = module_info
    # clear and build
    remove_previous_build()
    build_and_install()
    
    install_location = "../.venv/lib/python3.8/site-packages/"



    for file, test in files_to_test.items():
        # clear
        print(f'{file:^50s}', end='', flush=True)
        run("rm .coverage")
        run("rm .mutmut-cache")
        # coverage
        run(f"coverage run --source=midb -m pytest -x {test}")
        stdout, stderr, return_code = run(f"coverage report | grep {file}")
        try:
            result_table = stdout.strip().split()
            coverage = result_table[-1]
            miss = result_table[-2]
            statements = result_table[-3]
            coverage_report = f'{coverage} ({miss}/{statements})'
            print(f'{coverage_report:^15s}', end='', flush=True)
        except IndexError:
            print(run('coverage report').stdout)
            exit(1)
        # mutmut
        stdout, _, _ = run(f' mutmut run {install_location}{file} --runner "python -m pytest -x {test}"')
        mutmut_results = stdout.split('\n')[-2].split()
        try:
            survived = mutmut_results[mutmut_results.index('🙁') + 1]
            print(f'{survived:^15s}', end='', flush=True)
        except ValueError:
            print(f' mutmut run {install_location}{file} --runner "python -m pytest -x {test}"')
            print(stdout, stderr)
            exit(1)
        print()