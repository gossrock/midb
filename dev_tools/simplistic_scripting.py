# MIT License
#
# Copyright (c) 2021 Peter Goss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""
    The goal of simplistic_scripting.py is to be able to program the script in
    Python but run cli commands in Bash.
"""
from typing import NamedTuple
import subprocess


class Output(NamedTuple):
    stdout: str
    stderr: str
    return_code: int


def run(command: str, stdin: str = "", display=False):
    if display:
        print(command)
    process = subprocess.Popen(command, encoding='utf-8', stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               shell=True, executable='/bin/bash')
    stdout, stderr = process.communicate(stdin)
    if display and stdout.strip() != "":
        print(stdout)
    if display and stderr.strip() != "":
        print(stderr)
    return_code = process.wait()
    return Output(stdout, stderr, return_code)