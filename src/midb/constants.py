from midb.backend._serialization import ClassID
MEMORY_DB = ":memory:"  # pragma: no mutate
DEFAULT_ROOT_CLASS_ID = ClassID("midb.persistent_objects._pdict.PDict")  # pragma: no mutate
